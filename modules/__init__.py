import requests


def catch_name_and_url(list_of_images):
    links_of_images = []
    for i in range(0, len(list_of_images)):
        temp_dict = {}
        temp_dict["name"] = list_of_images[i]["tags"]
        temp_dict["url"] = list_of_images[i]["image"]
        links_of_images.append(temp_dict)
    return links_of_images


def download_files(link_of_images):
    for j in link_of_images:
        name_of_image = open(f'imagem{j["name"]}.jpg', 'wb')
        resp = requests.get(j["url"])
        name_of_image.write(resp.content)
        name_of_image.close()


def do_url(image):
    return 'https://list.ly/api/v4/search/image?q=' + image